#!/bin/sh
set -e

cd atc-git
./gradlew build
cp build/libs/air-traffic-control-0.0.1-SNAPSHOT.jar ../build/atc.jar