package io.pivotal.pipelinepracticum.airtrafficcontrol

import org.springframework.web.bind.annotation.*

@RestController
class StatusController {
    val statusRepo: MutableList<Status> = mutableListOf(
            Status("KS 991", "2018-11-30", "Delayed"),
            Status("KS 992", "2018-12-31", "On Time"),
            Status("KS 999", "2018-11-30", "Delayed"),
            Status("KS 999", "2018-12-31", "On Time")
    )

    @PostMapping("/status")
    fun createStatus(@RequestBody status: Status): Status {
        statusRepo.add(status)
        return status
    }

    @GetMapping("/statuses/{date}")
    fun getStatuses(@PathVariable("date") date: String): List<Status> {
        return statusRepo.filter { it.date == date }
    }
}

data class Status(
        val flightNumber: String,
        val date: String,
        val status: String
)
