package io.pivotal.pipelinepracticum.airtrafficcontrol

import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDate

@RestController
class BennysBushplanesController {
    val flightRepo: MutableList<Flight> = mutableListOf(
            Flight("KS 777", "2018-11-30"),
            Flight("KS 776", "2018-12-31")
    )

    @PostMapping("bennysbushplanes/flights")
    fun createFlight(@RequestBody flight: Flight): Flight {
        flightRepo.add(flight)
        return flight
    }

    @GetMapping("bennysbushplanes/flights")
    fun getFlights(): Map<Int, Map<String, Map<Int, List<String>>>> {
        /*
        {
            2018: {
                "January": {
                    13: [
                        "BB 123",
                        "BB 456"
                    ]
                }
            }
        }
         */
        return flightRepo
                .groupBy { LocalDate.parse(it.date).year }
                .mapValues { flightEntry ->
                    flightEntry.value
                            .groupBy { flight -> LocalDate.parse(flight.date).month.name }
                            .mapValues { monthEntry ->
                                monthEntry.value
                                        .groupBy { flight -> LocalDate.parse(flight.date).dayOfMonth }
                                        .mapValues { dayEntry -> dayEntry.value.map { it.flightNumber } }
                            }
                }
    }
}

data class Flight(
        val flightNumber: String,
        val date: String
)