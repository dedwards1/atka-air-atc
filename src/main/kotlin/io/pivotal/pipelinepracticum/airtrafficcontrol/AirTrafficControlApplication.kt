package io.pivotal.pipelinepracticum.airtrafficcontrol

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class AirTrafficControlApplication

fun main(args: Array<String>) {
    runApplication<AirTrafficControlApplication>(*args)
}
